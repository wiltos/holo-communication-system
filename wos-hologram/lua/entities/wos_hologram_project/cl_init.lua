ENT.Type 		= "anim"
ENT.PrintName	= "Hologram Table"
ENT.Author		= "King David"
ENT.Contact		= ""
ENT.Category = "wiltOS Technologies"
ENT.Spawnable			= true
ENT.RenderGroup 		= RENDERGROUP_OPAQUE

include('shared.lua')

local holomaterial = Material( "Models/effects/vol_light001", "unlitgeneric" )

ENT.DrawWeapon = false

function ENT:Initialize()

	self:SetModel( "models/niksacokica/tech/tech_holotable_01.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetCollisionGroup( COLLISION_GROUP_NONE )
	self:SetRenderBounds( Vector( -200, -200, 0 ), Vector( 200, 200, 200 ) )
	
	local phys = self:GetPhysicsObject()
	if IsValid( phys ) then
		phys:Wake()
	end
	
	self:Activate()
	
end

function ENT:Think()

end

local function HoloOverwrite( et )

	local orig_pos = et:GetPos()
	if et:IsPlayer() and et:InVehicle() then
		orig_pos = et:GetVehicle():GetPos() 
	end
	et:SetPos( et.WOS_HOLO_RENDERPOS )
	et:SetupBones()
	et:DrawModel()
	
	if et:IsPlayer() and et:GetActiveWeapon():IsValid() then
		local wep = et:GetActiveWeapon()
		wep:DrawModel()
	end

	et:SetPos( orig_pos )
	et:SetupBones()
end

hook.Add( "PreDrawOpaqueRenderables", "wOS.HoloStuff.PreventDraw", function( depth )
	
end )

function ENT:Draw()
	self.Entity:DrawModel()

	if self:GetEnabled() and self:GetUser():IsValid() then
		local cdist = GetConVar(  "wos_holo_drawdist" ):GetInt()
		if LocalPlayer():GetPos():DistToSqr( self:GetPos() ) > cdist^2 then return end
		if self:GetUser():GetPos():DistToSqr( self:GetPos() ) > 500000 then
			if self:GetUser() != LocalPlayer() then
				local min, max = self:GetCollisionBounds()
				min = min + self:GetUp()*35
				max = max + self:GetUp()*185
				
				local render_origin = self:GetPos() + self:GetUp()*35
				render.SetStencilWriteMask( 0xFF )
				render.SetStencilTestMask( 0xFF )
				render.SetStencilReferenceValue( 0 )
				render.SetStencilPassOperation( STENCIL_KEEP )
				render.SetStencilZFailOperation( STENCIL_KEEP )
				render.ClearStencil()

				render.SetStencilEnable( true )
				render.SetStencilReferenceValue( 1 )
				render.SetStencilCompareFunction( STENCIL_NEVER )
				render.SetStencilFailOperation( STENCIL_REPLACE )
				
				render.SetMaterial( holomaterial )
				render.DrawBox( self:GetPos(), self:GetAngles(), min, max, color_white )
				draw.NoTexture()
			
				render.SetStencilCompareFunction( STENCIL_EQUAL )
				render.SetStencilFailOperation( STENCIL_KEEP )
				
				render.SuppressEngineLighting( true )
				render.SetColorModulation( 0.65, 1, 1 )
				
				local ref_pos = self:GetUser():GetPos()
				local stop_player = false
				local cscale = GetConVar(  "wos_holo_camscale" ):GetFloat()
				if cscale  <= 0 then cscale  = 1 end
				local dist = 100
				if self:GetUser():IsPlayer() then
					if self:GetUser():InVehicle() then
						orig_pos = self:GetUser():GetVehicle():GetPos() 
					elseif IsValid( self:GetUser():GetViewEntity() ) then
						--[[
							Star wars vehicles support
							who the fuck wrote this thing
							this shit needs to get rewritten asap
							I'm talking about the vehicle base, not the crazy shit I had to do to get it working
						]]--
						local typ = self:GetUser():GetViewEntity().Vehicle
						if typ then
							if self:GetUser():GetNetworkedEntity( typ ) == self:GetUser():GetViewEntity() then
								ref_pos = self:GetUser():GetViewEntity():GetPos()
								stop_player = true
							end
						end
					end
				else
					stop_player = true
					dist = 300
				end
				dist = dist*cscale 
				for _, ent in ipairs( ents.FindInSphere( ref_pos, dist ) ) do
					if ent == self:GetUser() and stop_player then continue end
					local offset = ent:GetPos() - ref_pos
					ent.WOS_HOLO_LastCustomRender = CurTime() + 0.1
					ent.WOS_HOLO_RENDERPOS = render_origin + offset 
					ent.RenderOverride = HoloOverwrite
					
					render.SetBlend( 0.45 )
					ent:DrawModel()
					
					render.SetBlend( 0.05 )
					ent:DrawModel()
					
					render.SetBlend( 1 )
					ent.RenderOverride = nil
				end
				
				render.SetStencilEnable( false )
				render.SuppressEngineLighting( false )
			
			else
				local ang = Angle( 0, CurTime()*100%360, 90 )
				cam.Start3D2D( self:GetPos() + self:GetUp()*90, ang, 0.1 )
					draw.SimpleText( "YOU ARE CURRENTLY IN THIS CALL", "wOS.HoloStuff.3DFont", 0, 0, Color( 255, 0, 0 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
				cam.End3D2D()
				
				ang = Angle( 180, CurTime()*100%360, -90 )
				cam.Start3D2D( self:GetPos() + self:GetUp()*90, ang, 0.1 )
					draw.SimpleText( "YOU ARE CURRENTLY IN THIS CALL", "wOS.HoloStuff.3DFont", 0, 0, Color( 255, 0, 0 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
				cam.End3D2D()			
			end
		else
			local ang = Angle( 0, CurTime()*100%360, 90 )
			cam.Start3D2D( self:GetPos() + self:GetUp()*90, ang, 0.1 )
				draw.SimpleText( "HOLOGRAM DISABLED: TARGET IN RANGE", "wOS.HoloStuff.3DFont", 0, 0, Color( 255, 0, 0 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
			cam.End3D2D()
			
			ang = Angle( 180, CurTime()*100%360, -90 )
			cam.Start3D2D( self:GetPos() + self:GetUp()*90, ang, 0.1 )
				draw.SimpleText( "HOLOGRAM DISABLED: TARGET IN RANGE", "wOS.HoloStuff.3DFont", 0, 0, Color( 255, 0, 0 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
			cam.End3D2D()			
		end
		
		local name = "Holo Table"
		if self:GetUser():IsPlayer() then
			name = self:GetUser():Nick()
		else	
			if self:GetUser():GetClass() == "wos_hologram_project" then
				name = self:GetUser():GetTableName()
				dist = 300
			end
		end
		
		local ang = self:GetAngles()
		ang:RotateAroundAxis( ang:Up(), 0 )
		ang:RotateAroundAxis( ang:Forward(), 90 )
		cam.Start3D2D( self:GetPos() + self:GetUp()*45 + self:GetRight()*80, ang, 0.1 )
		draw.SimpleText( name, "wOS.HoloStuff.3DFont", 0, 0, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
		cam.End3D2D()

		ang:RotateAroundAxis( ang:Forward(), -180 )
		ang:RotateAroundAxis( ang:Up(), -180 )
		cam.Start3D2D( self:GetPos() + self:GetUp()*45 + self:GetRight()*-80, ang, 0.1 )
		draw.SimpleText( name, "wOS.HoloStuff.3DFont", 0, 0, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
		cam.End3D2D()
		
	end
	
end

function ENT:OnRemove()
	
end