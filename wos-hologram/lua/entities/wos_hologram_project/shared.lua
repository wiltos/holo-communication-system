ENT.Type 		= "anim"
ENT.PrintName	= "Hologram Table" 
ENT.Author		= "King David"
ENT.Contact		= ""
ENT.Category = "wiltOS Technologies"
ENT.Spawnable			= true
ENT.Editable = true

function ENT:SetupDataTables()
	self:NetworkVar( "Bool", 0, "Enabled" )
	self:NetworkVar( "Entity", 0, "User" )
	self:NetworkVar( "Vector", 0, "HoloPos" )
	self:NetworkVar( "String", 0, "TableName", { KeyName = "tablename", Edit = { type = "Generic" } } )
end