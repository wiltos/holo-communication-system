ENT.Type 		= "anim"
ENT.PrintName	= "Hologram Table"
ENT.Author		= "King David"
ENT.Contact		= ""
ENT.Category = "wiltOS Technologies"
ENT.Spawnable			= true

AddCSLuaFile('shared.lua')
AddCSLuaFile('cl_init.lua')
include( 'shared.lua' )

function ENT:Initialize()
	self:SetModel( "models/niksacokica/tech/tech_holotable_01.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetCollisionGroup( COLLISION_GROUP_NONE )
	self:SetUseType( SIMPLE_USE )
	self:SetTrigger( true )
	self:UseTriggerBounds( true, 900 )
	self:SetTableName( "Holo Table " .. self:EntIndex() )
	
	local phys = self:GetPhysicsObject()
	if IsValid( phys ) then
		phys:Wake()
	end
	
	self:Activate()
	
end

function ENT:Think()
	if not self.BuildingSound then
		self.BuildingSound = CreateSound( self.Entity, "ambient/machines/combine_shield_loop3.wav" )
		self.BuildingSound:Play()
	end
	
	if IsValid( self:GetUser() ) then
		local pos = self:GetUser():GetPos()
		if self:IsPlayer() then
			if not self:GetUser():Alive() then
				self:SetEnabled( false )
				self:SetUser( NULL )
				return
			end

			if IsValid( self:GetUser():GetViewEntity() ) then
				pos = self:GetUser():GetViewEntity():GetPos()
			end
			self:SetHoloPos( pos )
		end
	else
		self:SetEnabled( false )
		self:SetUser( NULL )
	end
	
end

function ENT:StartTouch( ent )
	if not ent:IsPlayer() then return end
	if not ent:Alive() then return end
	if ent == self:GetUser() then return end
	
	if IsValid( ent.WOS_HoloTable ) then return end
	
	ent.WOS_HoloTable = self
	
end

function ENT:EndTouch( ent )
	if not ent:IsPlayer() then return end
	if not ent:Alive() then return end
	if ent == self:GetUser() then return end
	
	if !IsValid( ent.WOS_HoloTable ) then return end
	if ent.WOS_HoloTable != self then return end
	
	ent.WOS_HoloTable = nil
	
end


function ENT:Use( activator, caller )

	if not activator:IsPlayer() then return end
	
	net.Start( "wOS.HoloStuff.SendMenu" )
		net.WriteEntity( self )
	net.Send( activator )
	
end

function ENT:OnRemove()
	self.BuildingSound:Stop()
end