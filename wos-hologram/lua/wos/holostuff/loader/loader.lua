
local root = "wos/holostuff"

if SERVER then
	AddCSLuaFile( root .. "/core/cl_core.lua" )
	AddCSLuaFile( root .. "/core/cl_net.lua" )
	AddCSLuaFile( root .. "/core/sh_core.lua" )
end

include( root .. "/core/sh_core.lua" )

if SERVER then
	include( root .. "/core/sv_core.lua" )
	include( root .. "/core/sv_net.lua" )
else
	include( root .. "/core/cl_core.lua" )
	include( root .. "/core/cl_net.lua" )
end