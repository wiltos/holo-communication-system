

CreateConVar( "wos_holo_drawdist", 700, { FCVAR_REPLICATED, FCVAR_ARCHIVE }, "What is the max distance a player can be before the hologram fades? Default 700" )
CreateConVar( "wos_holo_camscale", 1, { FCVAR_REPLICATED, FCVAR_ARCHIVE }, "what scale around the target should be visible? Note: The holograms will still only appear on the table" )
