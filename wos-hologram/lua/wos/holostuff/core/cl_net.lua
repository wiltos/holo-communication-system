

net.Receive( "wOS.HoloStuff.SendMenu", function()
	
	local holotable = net.ReadEntity()
	if not IsValid( holotable ) then return end
	wOS.HoloStuff:ToggleHoloMenu( holotable )

end )

net.Receive( "wOS.HoloStuff.SendError", function()

	local err = net.ReadString()
	if #err < 1 then return end

	surface.PlaySound( "buttons/lightswitch2.wav" )
	notification.AddLegacy( err, NOTIFY_ERROR, 3 )

end )

net.Receive( "wOS.HoloStuff.SendMessage", function()

	local err = net.ReadString()
	if #err < 1 then return end

	surface.PlaySound( "buttons/lightswitch2.wav" )
	notification.AddLegacy( err, NOTIFY_GENERIC, 3 )

end )

net.Receive( "wOS.HoloStuff.RequestCall", function()

	local holo = net.ReadEntity()
	local invite = net.ReadString()
	
	wOS.HoloStuff:CreateInviteMenu( holo, invite )

end )

net.Receive( "wOS.HoloStuff.RequestTables", function()

	local holo = net.ReadEntity()
	local tbl = net.ReadTable()
	
	wOS.HoloStuff:OpenCallMenu( holo, tbl )

end )