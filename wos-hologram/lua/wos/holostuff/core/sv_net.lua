

util.AddNetworkString( "wOS.HoloStuff.SendMenu" )
util.AddNetworkString( "wOS.HoloStuff.RequestCall" )
util.AddNetworkString( "wOS.HoloStuff.AcceptCall" )
util.AddNetworkString( "wOS.HoloStuff.Disconnect" )
util.AddNetworkString( "wOS.HoloStuff.SendError" )
util.AddNetworkString( "wOS.HoloStuff.SendMessage" )
util.AddNetworkString( "wOS.HoloStuff.RequestTables" )

net.Receive( "wOS.HoloStuff.RequestCall", function( len, ply )
	local holotable = net.ReadEntity()
	if not IsValid( holotable ) then return end
	if not holotable:GetClass() == "wos_hologram_project" then return end
	
	if holotable:GetEnabled() then 
		wOS.HoloStuff:SendErrorMessage( ply, "The Holo Table is currently in a call!" )
		return
	end
	
	local rply = net.ReadEntity()

	if not IsValid( rply ) then return end
	if not rply:IsPlayer() and rply:GetClass() != "wos_hologram_project" then return end
	if rply:IsPlayer() then
		local wep = rply:GetWeapon( "weapon_wos_holocaller" )
		if not IsValid( wep ) then 
			wOS.HoloStuff:SendErrorMessage( ply, "This player does not have a Holo Transceiver" )
			return	
		end
		
		if wep.InCall then
			wOS.HoloStuff:SendErrorMessage( ply, "This player is currently in a call" )
			return		
		end

		wep.CallData = { caller = ply, tbl = holotable, time = CurTime() + 10 }
		wOS.HoloStuff:SendMessage( rply, ply:Nick() .. " has invited you to a call. Use PRIMARY ATTACK to accept and SECONDARY ATTACK to decline"  )
		wOS.HoloStuff:SendMessage( ply, "Request sent to player " .. rply:Nick() )
		return
	else
		if rply:GetEnabled() then
			wOS.HoloStuff:SendErrorMessage( ply, "This table is currently in a call" )
			return	
		end
		wOS.HoloStuff:ConnectCall( rply, holotable )
		wOS.HoloStuff:SendMessage( ply, "Call has been connected!" )
		return
	end
		
end )

net.Receive( "wOS.HoloStuff.Disconnect", function( len, ply )
	local holotable = net.ReadEntity()
	if not IsValid( holotable ) then return end
	if not holotable:GetClass() == "wos_hologram_project" then return end
	
	if !holotable:GetEnabled() then 
		wOS.HoloStuff:SendErrorMessage( ply, "The Holo Table is not currently active!" )
		return
	end
	
	local rply = holotable:GetUser()
	wOS.HoloStuff:DisconnectCall( rply, holotable )
	wOS.HoloStuff:SendMessage( ply, "You have disconnected all calls!" )
	
end )

net.Receive( "wOS.HoloStuff.RequestTables", function( len, ply )

		local holo = net.ReadEntity()
		if not IsValid( holo ) then return end
		if not holo:GetClass() == "wos_hologram_project" then return end
		
		local tbls = {}
		for _, ent in ipairs( ents.FindByClass( "wos_hologram_project" ) ) do
			if ent == holo then continue end
			table.insert( tbls, ent )
		end
		net.Start( "wOS.HoloStuff.RequestTables" )
			net.WriteEntity( holo )
			net.WriteTable( tbls )
		net.Send( ply )
		
end )