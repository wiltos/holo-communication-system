

wOS = wOS or {}
wOS.HoloStuff = wOS.HoloStuff or {}

function wOS.HoloStuff:SendErrorMessage( ply, err )

	if not ply then return end
	if not err then return end
	
	net.Start( "wOS.HoloStuff.SendError" )
		net.WriteString( "[wOS - Holo] " .. err )
	net.Send( ply )
	
end

function wOS.HoloStuff:SendMessage( ply, message )

	if not ply then return end
	if not ply:IsPlayer() then return end
	if not message then return end
	
	net.Start( "wOS.HoloStuff.SendMessage" )
		net.WriteString( "[wOS - Holo] " .. message )
	net.Send( ply )
	
end

function wOS.HoloStuff:ConnectCall( ply, holotable )
	if not ply then return end
	if not holotable then return end
	
	if ply:IsPlayer() then
		local wep = ply:GetWeapon( "weapon_wos_holocaller" )
		if not IsValid( wep ) then 
			wOS.HoloStuff:SendErrorMessage( ply, "This player does not have a Holo Transceiver" )
			return	
		end
		
		if wep.InCall then
			wOS.HoloStuff:SendErrorMessage( ply, "This player is currently in a call" )
			return		
		end	
		wep.InCall = true
	else
		ply:SetUser( holotable )
		ply:SetEnabled( true )
	end
	
	if holotable:GetEnabled() then 
		wOS.HoloStuff:SendErrorMessage( ply, "The Holo Table is currently in a call!" ) 
		return
	end
	
	holotable:SetUser( ply )
	holotable:SetEnabled( true )
	
end

function wOS.HoloStuff:DisconnectCall( ply, holotable )
	
	if not ply then return end
	if not holotable then return end
	if not holotable:GetEnabled() then return end
	if holotable:GetUser() != ply then return end
	
	if ply:IsPlayer() then
		local wep = ply:GetWeapon( "weapon_wos_holocaller" )
		if IsValid( wep ) then 
			wep.InCall = false
			wep.CallData = nil
			wOS.HoloStuff:SendMessage( ply, "You have disconnected from the call!" )	
		end		
	else
		ply:SetEnabled( false )
		ply:SetUser( NULL )
	end
	
	holotable:SetUser( NULL )
	holotable:SetEnabled( false )
	
end

hook.Add( "SetupPlayerVisibility", "wOS.HoloStuff.CreatePVS", function( ply, viewent )
	if not ply:Alive() then return end
	if not ply.WOS_HoloTable then return end
	if not IsValid( ply.WOS_HoloTable ) then ply.WOS_HoloTable = nil return end
	if not ply.WOS_HoloTable:GetEnabled() then return end
	if not ply.WOS_HoloTable:GetUser():IsValid() then return end
	AddOriginToPVS( ply.WOS_HoloTable:GetHoloPos() )
end )

hook.Add( "PlayerCanHearPlayersVoice", "wOS.HoloStuff.EnableVoice", function( listener, talker )
	if not talker then return end
	for _, ent in ipairs( ents.FindByClass( "wos_hologram_project" ) ) do
		if not ent:GetEnabled() then continue end
		if not ent:GetUser():IsValid() then continue end
		if ent:GetUser() != talker then continue end
		if listener:GetPos():DistToSqr( ent:GetPos() ) < 200000 then return true end
	end
end )

hook.Add( "PlayerCanSeePlayersChat", "wOS.HoloStuff.EnableChat", function( _, __, listener, talker )
	if not talker then return end
	for _, ent in ipairs( ents.FindByClass( "wos_hologram_project" ) ) do
		if not ent:GetEnabled() then continue end
		if not ent:GetUser():IsValid() then continue end
		if ent:GetUser() != talker then continue end
		if listener:GetPos():DistToSqr( ent:GetPos() ) < 200000 then return true end
	end
end )