
wOS = wOS or {}
wOS.HoloStuff = wOS.HoloStuff or {}

local w, h = ScrW(), ScrH()

function wOS.HoloStuff:OpenCallMenu( holotable, tbl_overwrite )

	if not self.MainMenu then return end
	self.MainMenu:Clear()
	
	self.MainMenu.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 0, 0, 0, 155 ) )
		draw.SimpleText( "Calling Card", "DermaDefault", ww/2, hh*0.1, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end
	
	local mw, mh = self.MainMenu:GetSize()
	local padx, pady = mw*0.02, mh*0.02
	
	local closebutton = vgui.Create( "DButton", self.MainMenu )
	closebutton:SetPos( mw - padx - mh*0.04, pady )
	closebutton:SetSize( mh*0.04, mh*0.04 )
	closebutton:SetText( "" )
	closebutton.Paint = function( pan, ww, hh )
		surface.SetDrawColor( Color( 255, 0, 0 ) )
		surface.DrawLine( 0, hh, ww, 0 )
		surface.DrawLine( 0, 0, ww, hh )
	end
	closebutton.DoClick = function( pan )
		wOS.HoloStuff:ToggleHoloMenu( nil, true )
	end
	
	local playerlist = vgui.Create( "DListView", self.MainMenu )
	playerlist:SetPos( padx, mh*0.2 )
	playerlist:SetSize( mw - 2*padx, mh*0.6 )
	local col = playerlist:AddColumn( "Player" )
	col:SetTextAlign( 5 )
	
	playerlist.Players = tbl_overwrite or table.Copy( player.GetAll() )
	playerlist.NPlayers = {}
	for _, ply in ipairs( playerlist.Players ) do
		local name = "NAME ERROR"
		if ply:IsPlayer() then
			if not ply:HasWeapon( "weapon_wos_holocaller" ) then continue end
			name = ply:Nick()
		else
			name = ply:GetTableName()
		end
		playerlist.NPlayers[ #playerlist.NPlayers + 1 ] = ply
		playerlist:AddLine( name )
	end
	
	local callbutton = vgui.Create( "DButton", self.MainMenu )
	callbutton:SetPos( mw*0.7 - padx, mh*0.8 + pady )
	callbutton:SetSize( mw*0.3, mh*0.075 )
	callbutton:SetText( "" )
	callbutton.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 0, 255, 0, 255 ) )
		draw.SimpleText( "CALL PLAYER", "DermaDefault", ww/2, hh/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )		
	end
	callbutton.DoClick = function( pan )
		local line = playerlist:GetSelectedLine()
		if not line then return end
		if line < 1 then return end
		local ply = playerlist.NPlayers[ line ]
		if not IsValid( ply ) then return end
		net.Start( "wOS.HoloStuff.RequestCall" )
			net.WriteEntity( holotable )
			net.WriteEntity( ply )
		net.SendToServer()
		wOS.HoloStuff:ToggleHoloMenu( nil, true )
	end
	
	local hangbutton = vgui.Create( "DButton", self.MainMenu )
	hangbutton:SetPos( padx, mh*0.8 + pady )
	hangbutton:SetSize( mw*0.3, mh*0.075 )
	hangbutton:SetText( "" )
	hangbutton.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 255, 0, 0, 255 ) )
		draw.SimpleText( "GO BACK", "DermaDefault", ww/2, hh/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
	end
	hangbutton.DoClick = function( pan )
		wOS.HoloStuff:ToggleHoloMenu( holotable )
	end	

end

function wOS.HoloStuff:ToggleHoloMenu( holotable, close )
	
	if self.MainMenu or not holotable then
		self.MainMenu:Remove()
		self.MainMenu = nil
		if close then
			gui.EnableScreenClicker( false )
			return
		end
	end
	
	self.MainMenu = vgui.Create( "DPanel" )
	self.MainMenu:SetSize( w*0.4, h*0.4 )
	self.MainMenu:Center()
	self.MainMenu.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 0, 0, 0, 155 ) )
		draw.SimpleText( "Main Menu", "DermaDefault", ww/2, hh*0.1, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end
	self.MainMenu.Think = function( pan )
		if pan:IsVisible() then
			gui.EnableScreenClicker( true )
		end
	end

	local mw, mh = self.MainMenu:GetSize()
	local padx, pady = mw*0.02, mh*0.02
	
	local callbutton = vgui.Create( "DButton", self.MainMenu )
	callbutton:SetPos( mw*0.35, mh*0.2 )
	callbutton:SetSize( mw*0.3, mh*0.075 )
	callbutton:SetText( "" )
	callbutton.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 0, 255, 0, 255 ) )
		draw.SimpleText( "CALL PLAYER", "DermaDefault", ww/2, hh/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )		
	end
	callbutton.DoClick = function( pan )
		wOS.HoloStuff:OpenCallMenu( holotable )
	end
	
	local callbutton = vgui.Create( "DButton", self.MainMenu )
	callbutton:SetPos( mw*0.35, mh*0.3 )
	callbutton:SetSize( mw*0.3, mh*0.075 )
	callbutton:SetText( "" )
	callbutton.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 0, 255, 0, 255 ) )
		draw.SimpleText( "CALL HOLO TABLE", "DermaDefault", ww/2, hh/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )		
	end
	callbutton.DoClick = function( pan )
		net.Start( "wOS.HoloStuff.RequestTables" )
			net.WriteEntity( holotable )
		net.SendToServer()
	end
	
	local hangbutton = vgui.Create( "DButton", self.MainMenu )
	hangbutton:SetPos( mw*0.35, mh*0.6 )
	hangbutton:SetSize( mw*0.3, mh*0.075 )
	hangbutton:SetText( "" )
	hangbutton.Paint = function( pan, ww, hh )
		draw.RoundedBox( 8, 0, 0, ww, hh, Color( 255, 0, 0, 255 ) )
		draw.SimpleText( "HANG UP", "DermaDefault", ww/2, hh/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )				
	end
	hangbutton.DoClick = function( pan )
		net.Start( "wOS.HoloStuff.Disconnect" )
			net.WriteEntity( holotable )
		net.SendToServer()
	end	
	
	local closebutton = vgui.Create( "DButton", self.MainMenu )
	closebutton:SetPos( mw - padx - mh*0.04, pady )
	closebutton:SetSize( mh*0.04, mh*0.04 )
	closebutton:SetText( "" )
	closebutton.Paint = function( pan, ww, hh )
		surface.SetDrawColor( Color( 255, 0, 0 ) )
		surface.DrawLine( 0, hh, ww, 0 )
		surface.DrawLine( 0, 0, ww, hh )
	end
	closebutton.DoClick = function( pan )
		wOS.HoloStuff:ToggleHoloMenu( nil, true )
	end
	
end

surface.CreateFont( "wOS.HoloStuff.3DFont", {
	font = "Arial", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	extended = false,
	size = 100,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )