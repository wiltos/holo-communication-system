if (SERVER) then

	SWEP.Weight				= 5
	SWEP.AutoSwitchTo			= false
	SWEP.AutoSwitchFrom		= false
end

if ( CLIENT ) then
	SWEP.PrintName			= "Hologram Transceiver"	
	SWEP.DrawAmmo 			= false
	SWEP.DrawCrosshair 		= false
	SWEP.ViewModelFOV			= 77
	SWEP.ViewModelFlip		= false
	SWEP.CSMuzzleFlashes		= true
	
	SWEP.Slot				= 2
	SWEP.SlotPos			= 0
	SWEP.IconLetter			= "j"
end

SWEP.Category			= "[wOS] Hologram"

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.DrawWeaponInfoBox  	= false

SWEP.Weight					= 5
SWEP.AutoSwitchTo				= false
SWEP.AutoSwitchFrom			= false

SWEP.Primary.ClipSize			= -1
SWEP.Primary.Damage			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic			= true
SWEP.Primary.Ammo			="none"


SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Damage			= -1
SWEP.Secondary.Automatic		= true
SWEP.Secondary.Ammo			="none"

SWEP.DrawWorldModel = false
SWEP.InCall = false

/*---------------------------------------------------------
Think
---------------------------------------------------------*/
function SWEP:Think()
	if self.InCall then
		if self.CallData then
			if not IsValid( self.CallData.tbl ) then
				self.InCall = false
				self.CallData = nil
			end
		end
	end
end

/*---------------------------------------------------------
Initialize
---------------------------------------------------------*/
function SWEP:Initialize() 
	self:SetWeaponHoldType( "slam" ) 	 
end 

/*---------------------------------------------------------
Deploy
---------------------------------------------------------*/
function SWEP:Deploy()
	return true
end

/*---------------------------------------------------------
PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
	self:SetNextPrimaryFire( CurTime() + 1 )
	if SERVER then
		if !self.InCall then
			if not self.CallData then return end
			if self.CallData.time < CurTime() then
				self.CallData = nil
				wOS.HoloStuff:SendMessage( self.Owner, "Your last call has expired." )
				return
			end
			if not IsValid( self.CallData.tbl ) then
				wOS.HoloStuff:SendMessage( self.Owner, "The Holo Table doesn't exist." )
				self.CallData = nil
				return
			end
			wOS.HoloStuff:SendMessage( self.Owner, "You are now connected to the call!" )			
			wOS.HoloStuff:ConnectCall( self.Owner, self.CallData.tbl )
		else
			wOS.HoloStuff:SendMessage( self.Owner, "You are already in a call! Disconnect before accepting any other calls" )			
		end
	end
end

function SWEP:SecondaryAttack()
	self:SetNextSecondaryFire( CurTime() + 1 )
	if not SERVER then return end
	if not self.InCall then return end
	wOS.HoloStuff:DisconnectCall( self.Owner, self.CallData.tbl )
end

function SWEP:Reload()
	return false
end

/*---------------------------------------------------------
OnRemove
---------------------------------------------------------*/
function SWEP:OnRemove()
	return true
end

/*---------------------------------------------------------
Holster
---------------------------------------------------------*/
function SWEP:Holster()
	return true
end

if CLIENT then
	function SWEP:PreDrawViewModel() render.SetBlend(0) end
	function SWEP:DrawWorldModel() end
	function SWEP:DrawWeaponSelection(x,y,wide,tall,alpha) end
end